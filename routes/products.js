const express = require('express')
const router = express.Router()
const products = [
  { id: 1, name: 'IPad gen1 64G Wifi', price: 11000.0 },
  { id: 2, name: 'IPad gen2 64G Wifi', price: 12000.0 },
  { id: 3, name: 'IPad gen3 64G Wifi', price: 13000.0 },
  { id: 4, name: 'IPad gen4 64G Wifi', price: 14000.0 },
  { id: 5, name: 'IPad gen5 64G Wifi', price: 15000.0 },
  { id: 6, name: 'IPad gen6 64G Wifi', price: 16000.0 },
  { id: 7, name: 'IPad gen7 64G Wifi', price: 17000.0 },
  { id: 8, name: 'IPad gen8 64G Wifi', price: 18000.0 },
  { id: 9, name: 'IPad gen9 64G Wifi', price: 19000.0 },
  { id: 10, name: 'IPad gen10 64G Wifi', price: 20000.0 }
]
let lastId = 11
const getProducts = function (req, res, next) {
  res.json(products)
}
const getProduct = function (req, res, next) {
  const index = products.findIndex(function (item) {
    return item.id === parseInt(req.params.id)
  })
  if (index >= 0) {
    res.json(products[index])
  } else {
    res.status(404).json({
      code: 404,
      msg: 'No product id ' + req.params.id
    })
  }
}
const addProducts = function (req, res, next) {
  const newProduct = {
    id: lastId,
    name: req.body.name,
    price: parseFloat(req.body.price)
  }
  products.push(newProduct)
  lastId++
  res.status(201).json(newProduct)
}
const updateProduct = function (req, res, next) {
  const productId = parseInt(req.params.id)
  const product = {
    id: productId,
    name: req.body.name,
    price: parseFloat(req.body.price)
  }
  const index = products.findIndex(function (item) {
    return item.id === productId
  })
  if (index >= 0) {
    products[index] = product
    res.json(products[index])
  } else {
    res.status(404).json({
      code: 404,
      msg: 'No product id ' + req.params.id
    })
  }
}
const deleteProduct = function (req, res, next) {
  const productId = parseInt(req.params.id)
  const index = products.findIndex(function (item) {
    return item.id === productId
  })
  if (index >= 0) {
    products.splice(index, 1)
    res.status(200).send()
  } else {
    res.status(404).json({
      code: 404,
      msg: 'No product id ' + req.params.id
    })
  }
}

router.get('/', getProducts) // GET Products
router.get('/:id', getProduct) // GET One Product
router.post('/', addProducts) // Add New Product
router.put('/:id', updateProduct) // Add New Product
router.delete('/:id', deleteProduct)
module.exports = router
